### An Asynchronous HTTP Library for Android 

An asynchronous callback-based Http client for Android built on top of Apache’s HttpClient libraries. All requests are made outside of your app’s main UI thread, but any callback logic will be executed on the same thread as the callback was created using Android’s Handler message passing.



详细介绍：[http://loopj.com/android-async-http/](http://loopj.com/android-async-http/)

源码：[https://github.com/loopj/android-async-http](https://github.com/loopj/android-async-http)

